// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index'
import env from './env'
import axios from 'axios'
axios.defaults.baseURL = env.API_URL

window.axios = axios
Vue.config.productionTip = false
Vue.filter('toFixed', function (price, limit) {
  return price.toFixed(limit);
});

Vue.filter('toPLN', function (price) {
  price = price.replace('.',',');
  return `${price} zł`;
});

Vue.filter('formatDate', function (date) {
  price = price.replace('.',',');
  return `${price} zł`;
});

Vue.filter('upperCase', function(value) {
  return value[0].toUpperCase()+value.substring(1,value.length);
});

Vue.use(router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  env,
  router,
  template: '<App/>',
  components: { App },
})
