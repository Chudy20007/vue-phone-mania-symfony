const state = {
  roles: [],
  role: {},
  refresh: 0
}



const actions = {
  roles({
    commit
  }) {
    axios
      .get(`roles`,this.getters.header)
      .then(response => commit('ROLES', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });;
  },

  role({
    commit
  }, id) {
    axios
      .get(`roles/${id}`,this.getters.header)
      .then(response =>
        commit('ROLE', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });;
  },
  deleteRole({
    commit
  }, id) {
    axios.delete(`roles/${id}`,this.getters.header)
      .then(response => {
        commit('PUSHRESPONSE', response)
        commit('REFRESH')
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  storeRole({
    commit
  }, role) {
    axios.post(`/roles`, role,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      }).catch(error => {
        commit('PUSHERROR', error)
      });
  },
  updateRole({
    commit
  }, role) {
    axios.put(`/roles/${role.id}`, role,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  }

}

const mutations = {
  ROLES: (state, roles) =>
    state.roles = roles,
  ROLE: (state, role) =>
    state.role = role,
  REFRESH: state =>
    state.refresh = ++state.refresh

}

const getters = {
  roles: state => {
    return state.roles;
  },
  role: state => {
    return state.role;
  },

}

export default {
  state,
  getters,
  actions,
  mutations
}
