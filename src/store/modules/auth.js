const state = {
    logged:false,
    header:'',
    redirect:''
}

const getters = {
    logged:state => {
        return state.logged
    },
    redirect:state => {
        return state.redirect
    },
    header:state => {
        return state.header
    },
    token: state => {
        return state.token
    }

}
const actions = {
    login({commit},loginData){
        axios.post('login_check',loginData)
        .then(result => {
            if(result == undefined)
            throw new Error("Niepoprawny login lub hasło!");
                var config = {
                    headers: {'Authorization': " Bearer " + result.data.token }
                };
                   commit('HEADER',config);
                   commit('LOGGED',true);
                   
                   commit('PUSHRESPONSE', result);
                   commit ('REFRESH');
                   switch(result.data.role[0]){
                       case 'ROLE_ADMIN': {
                        localStorage.setItem('role','administrator');
                        commit('USERROLE', 'administrator');
                        break;
                       }
                       case 'ROLE_WORKER': {
                        localStorage.setItem('role','worker');
                        commit('USERROLE', 'worker');
                        break;
                       }
                       case 'ROLE_AUTHOR': {
                        localStorage.setItem('role','author');
                        commit('USERROLE', 'author');
                        break;
                       }

                       case 'ROLE_USER': {
                        localStorage.setItem('role','user');
                        commit('USERROLE', 'user');
                           break;
                       }
                   }
                   localStorage.setItem('token',result.data.token)
                   
                  // commit('TOKEN',result.data.access_token);            
            }).catch(error => {
                console.log(error);
                commit('PUSHERROR', error)
              })
            },
    logout({commit}){
        axios.post('logout',null,this.getters.header)
        .then(result => {
            commit('LOGGED',false);
            commit('HEADER','');
            commit('PUSHRESPONSE', result);
            commit('USERROLE','');
            localStorage.removeItem('token');
            localStorage.removeItem('role');
        })
    },
    refreshToken({commit}){
        axios.get('authenticate/refresh',this.getters.header)
        .then(result =>{
                 var config = {
                     headers: {'Authorization': " Bearer " + result.data.access_token }
                 };
                 commit('HEADER',config);
                 commit('PUSHRESPONSE', result);
        })
    }
}

const mutations = {
    LOGGED(state,status){
        state.logged = status;
    },
    HEADER(state,header){
        state.header = header;
    },
    TOKEN(state, token){
        state.token = token;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}