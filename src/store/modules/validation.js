import {
  VALID,
  UNTESTED,
  INVALID
} from './const_types'

const state = {
  inputsNumber: [],
  validationPassed: false,

}

const actions = {
  validFormField({
    commit
  }, [value = document.getElementById("password").value, invalidInputArray, inputIndex, inputRegexPattern, checkbox = false]) {
    var reg = new RegExp(inputRegexPattern);
    if(!value && inputIndex === 9){
    commit('INPUT', [inputIndex, VALID]);
    return;
  }
    if (value === '' || value.length == 0) {
      commit('INPUT', [inputIndex, INVALID]);
      return;
    }
    if (!reg.test(value)) {
      commit('INPUT', [inputIndex, INVALID]);
      return;
    } else commit('INPUT', [inputIndex, VALID]);
  },

  validSelectFormField({commit},[invalidInputArray, inputIndex, select]){
    var selectedState = select.attributes['name'].value.toUpperCase();
    var value = select.value;

    if (value === '' || value.length == 0) {
      commit('INPUT', [inputIndex, INVALID]);
      return;
    }
    else{
      commit(selectedState,value)
      commit('INPUT', [inputIndex, VALID]);
    } 

  },
  validateForm({
    commit
  }) {
  
        if (state.inputsNumber.includes(INVALID) || state.inputsNumber.includes(UNTESTED))
        {
          
          state.validationPassed = false;
        }
        else
         state.validationPassed = true;
  
  },

  prepareArray({
    commit
  },[count, value]) {
    commit('INPUTSCREATE',[count, value]);
  },


}
const mutations = {
  EVENT: (state, event) =>
    state.event = event,
  INPUT: (state, [inputID, value]) =>
    state.inputsNumber[inputID] = value,

  INPUTSCREATE(state,[count, value]) {
    for (var i = 0; i < count; i++)
      state.inputsNumber[i] = value;
  },
  REFRESH: state =>
    state.refresh = ++state.refresh

}


const getters = {

  inputsNumber: state => {
    return state.inputsNumber;
  },
  validationPassed: state => {
    return state.validationPassed;
  },

}

export default {
  state,
  actions,
  mutations,
  getters
}
