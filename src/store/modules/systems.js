const state = {
  systems: [],
  system: {},
  refresh: 0
}

const actions = {
  systems({
    commit
  }) {
    axios
      .get('systems',this.getters.header)
      .then(response => commit('SYSTEMS', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  system({
    commit
  }, id) {
    axios
      .get(`systems/${id}`,this.getters.header)
      .then(response => commit('SYSTEM', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  updateSystem({
    commit
  }, system) {
    axios.put(`systems/${system.id}`, system,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  deleteSystem({
    commit
  }, id) {
    axios.delete(`systems/${id}`,this.getters.header)
    .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  storeSystem({
    commit
  }, system) {
    axios.post(`/systems`, system,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');


      }).catch(error => {
        commit('PUSHERROR', error)
      });
  }

}

const getters = {
  systems: state => {
    return state.systems;
  },

  system: state => {
    return state.system;
  },

}


const mutations = {
  SYSTEMS: (state, systems) =>
    state.systems = systems,
  SYSTEM: (state, system) =>
    state.system = system,
  REFRESH: state =>
    state.refresh = ++state.refresh
}

export default {
  state,
  actions,
  mutations,
  getters
}
