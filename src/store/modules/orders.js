const state = {
    orders: [],
    order: {},
    refresh: 0
  }
  
  const actions = {
    userOrders({
      commit
    }) {
      axios
        .get('user-orders',this.getters.header)
        .then(response => commit('ORDERS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    order({
      commit
    }, id) {
      axios
        .get(`orders/${id}`,this.getters.header)
        .then(response => commit('ORDER', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateOrder({commit}, order) {
      axios.put(`orders/${order.id}`, order,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    removeProductFromOrder({commit},productID){
      axios.delete(`orders/delete-product/${productID}`,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
    },
    deleteOrder({
      commit
    }, id) {
      axios.delete(`orders/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeOrder({
      commit
    }, order) {
      axios.post(`/orders`, order,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    orders: state => {
      return state.orders;
    },
  
    order: state => {
      return state.order;
    },
  
  }
  
  
  const mutations = {
    ORDERS: (state, orders) =>
      state.orders = orders,
    ORDER: (state, order) =>
      state.order = order,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  