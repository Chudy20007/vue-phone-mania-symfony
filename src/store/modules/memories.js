const state = {
    memories: [],
    memory: {},
    refresh: 0
  }
  
  const actions = {
    memories({
      commit
    }) {
      axios
        .get('memories',this.getters.header)
        .then(response => commit('MEMORIES', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    memory({
      commit
    }, id) {
      axios
        .get(`memories/${id}`,this.getters.header)
        .then(response => commit('MEMORY', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateMemory({
      commit
    }, memory) {
      axios.put(`memories/${memory.id}`, memory,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteMemory({
      commit
    }, id) {
      axios.delete(`memories/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeMemory({
      commit
    }, memory) {
      axios.post(`/memories`, memory,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    memories: state => {
      return state.memories;
    },
  
    memory: state => {
      return state.memory;
    },
  
  }
  
  
  const mutations = {
    MEMORIES: (state, memories) =>
      state.memories = memories,
    MEMORY: (state, memory) =>
      state.memory = memory,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  