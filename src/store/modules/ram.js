const state = {
  rams: [],
  ram: {},
  refresh: 0
}

const actions = {
  rams({
    commit
  }) {
    axios
      .get('rams',this.getters.header)
      .then(response => commit('RAMS', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  ram({
    commit
  }, id) {
    axios
      .get(`rams/${id}`,this.getters.header)
      .then(response => commit('RAM', response.data.data))
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  updateRam({
    commit
  }, ram) {
    axios.put(`rams/${ram.id}`, ram,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },

  deleteRam({
    commit
  }, id) {
    axios.delete(`rams/${id}`,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');
      })
      .catch(error => {
        commit('PUSHERROR', error)
      });
  },
  storeRam({
    commit
  }, ram) {
    axios.post(`/rams`, ram,this.getters.header)
      .then((response) => {
        commit('PUSHRESPONSE', response);
        commit('REFRESH');


      }).catch(error => {
        commit('PUSHERROR', error)
      });
  }

}

const getters = {
  rams: state => {
    return state.rams;
  },

  ram: state => {
    return state.ram;
  },

}


const mutations = {
  RAMS: (state, rams) =>
    state.rams = rams,
  RAM: (state, ram) =>
    state.ram = ram,
  REFRESH: state =>
    state.refresh = ++state.refresh
}

export default {
  state,
  actions,
  mutations,
  getters
}

