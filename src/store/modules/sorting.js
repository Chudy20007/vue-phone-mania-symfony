const state = {
    order_icons:[],
    order:1
}

const actions = {

    sortList ({commit},[selectedValue, selectedList]) { 

        if(selectedList.count != undefined || selectedList.length != undefined)
        {
            console.log('1');
            if(selectedValue.indexOf('.') >-1)
            {
                console.log('2');
                actions.sortListByRelationNumber({commit},[selectedValue, selectedList]);
              return;
            }
        if(mutations.isNumber(selectedList[0][selectedValue]) && selectedList.length)
        actions.sortListByNumber({commit},[selectedValue, selectedList]);
        else
        actions.sortListByString({commit},[selectedValue, selectedList]);
        }
    },
    prepareOrderIcons({commit}, count){
      commit('ORDERICONSCREATE',count);
    },

    sortListByString ({commit},[selectedValue, selectedList]) { 
            return selectedList.sort((a, b)=>{

                var x = a[selectedValue].toLowerCase();
              var y = b[selectedValue].toLowerCase();
              if (x < y) {return -1;}
              if (x > y) {return 1;}
              return 0;
          
          }); 
    },
    sortListByRelationNumber({commit},[selectedValue, selectedList]){
        var dotIndex = selectedValue.indexOf('.');
        var relation = selectedValue.slice(0,dotIndex);
        var relationValue = selectedValue.slice(dotIndex+1, selectedValue.length);
        return selectedList.sort((d1, d2) => d2[relation][relationValue] - d1[relation][relationValue]);

    },
    sortListByNumber ({commit},[selectedValue, selectedList]) { 

        return selectedList.sort((d1, d2) => d2[selectedValue] - d1[selectedValue]);
     }

}

const mutations = {
    isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    },
    ORDER: (state,columnIndex) => {
        state.order *= -1;
        mutations.ORDERICONSRESET(state);
        state.order_icons[columnIndex] = true;
        },
        ORDERICONSCREATE(state,count){
          for(var i=0; i < count; i++)   
          state.order_icons[i] = true;
        },
        ORDERICONSRESET(state){
          state.order_icons.forEach(function(val,index) {
           state.order_icons[index] = false;  
         }); 
        }
}

const getters = {
    order: state => {
        return state.order
      },
      orderIcons: state => {
        return state.order_icons
      }
}

export default {
    state,
    actions,
    mutations,
    getters
  }