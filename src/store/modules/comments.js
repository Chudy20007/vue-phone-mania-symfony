const state = {
    comments: [],
    comment: {},
    refresh: 0
  }
  
  const actions = {
    comments({
      commit
    }) {
      axios
        .get('comments',this.getters.header)
        .then(response => commit('COMMENTS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    comment({
      commit
    }, id) {
      axios
        .get(`comments/${id}`,this.getters.header)
        .then(response => commit('COMMENT', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateComment({
      commit
    }, comment) {
      axios.put(`comments/${comment.id}`, comment,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteComment({
      commit
    }, id) {
      axios.delete(`comments/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeComment({
      commit
    }, comment) {
      axios.post(`/comments`, comment,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    comments: state => {
      return state.comments;
    },
  
    comment: state => {
      return state.comment;
    },
  
  }
  
  
  const mutations = {
    COMMENTS: (state, comments) =>
      state.comments = comments,
    COMMENT: (state, comment) =>
      state.comment = comment,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  