const state = {
    cameras: [],
    camera: {},
    refresh: 0
  }
  
  const actions = {
    cameras({
      commit
    }) {
      axios
        .get('cameras',this.getters.header)
        .then(response => commit('CAMERAS', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    camera({
      commit
    }, id) {
      axios
        .get(`cameras/${id}`,this.getters.header)
        .then(response => commit('CAMERA', response.data.data))
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    updateCamera({
      commit
    }, camera) {
      axios.put(`cameras/${camera.id}`, camera,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
  
    deleteCamera({
      commit
    }, id) {
      axios.delete(`cameras/${id}`,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
        })
        .catch(error => {
          commit('PUSHERROR', error)
        });
    },
    storeCamera({
      commit
    }, camera) {
      axios.post(`/cameras`, camera,this.getters.header)
        .then((response) => {
          commit('PUSHRESPONSE', response);
          commit('REFRESH');
  
  
        }).catch(error => {
          commit('PUSHERROR', error)
        });
    }
  
  }
  
  const getters = {
    cameras: state => {
      return state.cameras;
    },
  
    camera: state => {
      return state.camera;
    },
  
  }
  
  
  const mutations = {
    CAMERAS: (state, cameras) =>
      state.cameras = cameras,
    CAMERA: (state, camera) =>
      state.camera = camera,
    REFRESH: state =>
      state.refresh = ++state.refresh
  }
  
  export default {
    state,
    actions,
    mutations,
    getters
  }
  
  